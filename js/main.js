angular.module('app',[])

.controller('qrcodeCtrl', function($scope,$http) {
    $scope.qrUrl = '';
    $scope.curTitle = '';
    $scope.curLink = '';
    $scope.show_error = 0;
    
    // looking in google for result. Now is set to look over Nsk.Could be changed into Engine settings
    $scope.gSearch = function(query) {
            $http.get('https://www.googleapis.com/customsearch/v1', {
                params: {
                    key: 'AIzaSyBhIhIDOlCddHoitjzATG4hTlmehEdkWh0',
                    cx: '015770317323660382718:4vuvjtkhjhs',
                    q: query
                }
            }).then(function(response) {
                $scope.items = response.data.items;
                if ($scope.items == undefined){
                    $scope.show_error = 1;
                }
                else {
                $scope.show_error = 0;
                }
            }); 
    };
    
    // Getting id from link and add to another link
    $scope.linkIdGet = function(link){
        $scope.qrUrl = "http://novosibirsk.flamp.ru/addreview/" + link.split('-')[1];
        location.href = "svg.html?target=" + $scope.qrUrl;
    }
    
    $scope.getObj = function(obj){
        $scope.curTitle = obj.title.split('(')[0];
        $scope.curLink = obj.link;
    }

});
