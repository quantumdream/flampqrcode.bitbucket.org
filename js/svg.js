angular.module('app',[])

.config( [
    '$compileProvider',
    function( $compileProvider )
    {   
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|data):/);
        // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
    }
])

.controller('svgCtrl', function($scope) {

    $scope.svg_url = 'data:image/svg;base64,' + btoa(document.getElementById('svg').innerHTML);
    $scope.png_url = ''; 
});
